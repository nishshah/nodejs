const users = []

//addUser
const addUser = ({ id,username,room })=>{
    //clean data
    username = username.trim().toLowerCase()
    room = room.trim().toLowerCase()

    //validate Data
    if(!username || !room){
        return {
            error:"UserName and Room Required"
        }
    }

    //Check for existing User
    const existingUser = users.find((user)=>{
        return user.room === room && user.username === username
    })

    //validate User Name
    if(existingUser){
        return {
            error:"Username Already taken use Different Name!"
        }
    }

    //storeUser
    const user = { id, username, room}
    users.push(user)
    return { user }
}


     
//removeUser

const removeUser = (id) =>{
    const index = users.findIndex ((user) => user.id == id)
    if (index !== -1) { 
         return users.splice(index, 1) [0]
    }
}
//getUser
 const getUser = (id) =>{
    return users.find((users)=> users.id == id)
    // console.log(user || undefined)
 }

//getUsersInRoom
const userInRoom = (room)=>{
    const user = users.filter((users)=>{ 
        room = room.trim().toLowerCase()
        return users.room === room
    })
    return user
    //console.log(user || undefined)
}

module.exports = {
    addUser,
    getUser,
    removeUser,
    userInRoom
}


//test

// addUser({
//     id: 22,
//     username: 'Andrew',
//     room: ' South Philly'
// })

// addUser({
//     id: 32,
//     username: 'Mike',
//     room: 'South Philly'
// })

// addUser({
//     id: 42,
//     username: 'Andrew',
//     room: 'Center City'
// })
// console.log(users)

// getUser(421)
// userInRoom('India')
// const res = addUser({
//     id: 33,
//     username: 'andrew',
//     room: 'India'
// })
// console.log(res)   

// const removedUser = removeUser(22)
// console.log(removedUser)
// console. log(users)  