const path = require('path')
const http = require('http')
const express = require('express')
const socketio = require('socket.io')
const  Filter = require('bad-words')
const { generateMessage, generatelocation } =  require('./utils/messages')
const { addUser, removeUser, getUser, userInRoom } = require('./utils/user')
const { get } = require('express/lib/response')


const app = express()
const server = http.createServer(app)
const io = socketio(server)

const port = process.env.PORT || 3000
const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))




io.on('connection',(socket)=>{
    // console.log("New connection");


    socket.on('join',({ username, room },callback)=>{
        const{ error, user }= addUser({ id: socket.id, username, room })

        if(error){
            return callback(error)
        }

        socket.join(user.room)
        socket.emit('message', generateMessage('Admin','Welcome!'))
        socket.broadcast.to(user.room).emit('message',generateMessage('Admin',`${user.username} has joined the Room!`))

        io.to(user.room).emit('roomData',{
            room: user.room,
            users: userInRoom(user.room)
        })
        callback()
    })

    socket.on('sendMessage', (message,callaback) => {
        const filter = new Filter()
        const user = getUser(socket.id)
        if(filter.isProfane(message)){
            return callaback('this words are not allowed!')
        }
        io.to(user.room).emit('message',generateMessage(user.username,message))
        callaback()
    })

    socket.on('sendLocation', ({lat,long},callaback) => {
        const user = getUser(socket.id)
        console.log(user)
        io.to(user.room).emit('sendLocation',generatelocation(user.username,`https://google.com/maps?q=${lat},${long}`))
        callaback();
    })
    
    socket.on('disconnect',()=>{
        const user = removeUser(socket.id)

        if(user){
            io.to(user.room).emit('message',generateMessage('Admin',`${user.username} has left!`))
            io.to(user.room).emit('roomData',{
                room: user.room,
                users: userInRoom(user.room)               
            })
        }
       
    })
})

//    const test= 
// console.log(test);
// console.log(message);

server.listen(port, () => {
    console.log(`Server is up on port ${port}!`)
})