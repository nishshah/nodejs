const socket =  io()

//Elements
const $messageForm =   document.querySelector('#message-form')
const $messageFormInput = $messageForm.querySelector('input')
const $messageFormButton = $messageForm.querySelector('button')
const $locationButton = document.querySelector('#sendLocation')
const $messages = document.querySelector('#messages')

// Templates
const messageTemplate = document.querySelector('#messageTemplate').innerHTML
const urlTemplate = document.querySelector("#urlTemplate").innerHTML
const sidebar = document.querySelector('#sideBarTemplate').innerHTML

// Options
 const { username, room } = Qs.parse(location.search, ({ignoreQueryPrefix: true }))

 const autoscroll = ()=>{
     //get new msges
    const $newMessage = $messages.lastElementChild

    //Height of new msg
    const newMessageStyles = getComputedStyle($newMessage)
    const newMessageMargin = parseInt(newMessageStyles.marginBottom)
    const newMessageHeight = $newMessage.offsetHeight + newMessageMargin

    // Visible height
    const visibleHeight = $messages.offsetHeight 
    // Height of messages container
    const containerHeight = $messages.scrollHeight
    // How far have I scrolled?
    const scrolloffset = $messages.scrollTop + visibleHeight
    if(containerHeight - newMessageHeight <= scrolloffset){
        $messages.scrollTop = $messages.scrollHeight
    }
 }
socket.on('message',(message)=>{
         console.log(message)

        const html = Mustache.render(messageTemplate,{
            name : message.username,
            message: message.text,
            createdAt: moment(message.createdAt).format("HH:mm a")
        })
        $messages.insertAdjacentHTML('beforeend',html)
        autoscroll();
    })

    socket.on('sendLocation',(url)=>{
        console.log(url)
        const html = Mustache.render(urlTemplate,{
            name: url.username,
            url : url.url,
            createdAt: moment(url.createdAt).format("HH:mm a")
        })
        $messages.insertAdjacentHTML('beforeend',html)
        autoscroll()
    })

    socket.on('roomData',({room, users})=>{
        // console.log(room)
        // console.log(users);
        const html = Mustache.render(sidebar,{
            room,
            users
        })
        document.querySelector('#sidebar').innerHTML = html
    })



    $messageForm.addEventListener('submit',(e)=>{
        e.preventDefault()
        $messageFormButton.setAttribute('disabled','disabled')
        const message =  e.target.elements.message.value
        socket.emit('sendMessage',message,(error)=>{
            $messageFormButton.removeAttribute('disabled')
            $messageFormInput.value= ''
            $messageFormInput.focus()
            if(error){
                return console.log(error);
            }
            console.log('Delivered!');
        })
    })
   

    $locationButton.addEventListener('click',()=>{
        if(!navigator.geolocation){
            return alert("Not Supproted by browser!")
        }
        $locationButton.setAttribute('disabled','disabled')
        navigator.geolocation.getCurrentPosition((postion)=>{
           const lat = postion.coords.latitude;
           const long = postion.coords.longitude;
           socket.emit('sendLocation',{lat,long},(error)=>{
            $locationButton.removeAttribute('disabled')
                if(error){
                    return console.log(error);
                }
                console.log('Location Shared!');
           })
        })
    })

    socket.emit('join', { username,room }, (error)=>{
        if(error){
            alert(error)
            location.href = "/"
        }
    })