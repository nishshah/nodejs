//express api call
const express = require('express');
const app = express();

//morgan for middleware
const morgan = require('morgan')

//bodyparser
const bodyparser = require('body-parser')

//database connection
const mongoose = require('mongoose')

//route files
const productRoutes = require('./api/routes/products')
const orderRoutes = require('./api/routes/orders')

//connect to cloud data base
mongoose.connect('mongodb+srv://ShaNish:'+ process.env.MONO_PW +'@cluster0.xywyf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
    useNewUrlParser: true
})

//use of morgan middleware
app.use(morgan('dev'))

//parsing data in url
app.use(bodyparser.urlencoded({extended: false}))
//parsing json data
app.use(bodyparser.json())

//CROS error resolutions
app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Header', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', ' GET, PUT, POST, PATCH, DELETE ')
        return res.status(200).json({})
    }
    next();
})


//Routes to handle request
app.use('/products', productRoutes )
app.use('/orders', orderRoutes)

//if request comes here that means upper handlers are showing error
app.use((req,res,next)=>{
    const error = new Error('Not Found!')
    error.status = 404
    next(error)
})

//any error thrown this middleware will get it in use
app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error:{
            message : error.message
        }
    })
})

//exporting app as a () to server to perform application
module.exports = app;