const { request } = require('express')
const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const multer = require('multer')
const upload = multer({dest:'uploads/'})

const Product = require('../models/product')

//GET method working on /product
router.get('/',(req,res,next)=>{
    
    Product.find()
            .select("name price _id")//Data we want show to the user
           .exec()
           .then(doc=> {
              const response ={
                count: doc.length,
                prosucts : doc.map(docs => {
                    return{
                        name: docs.name,
                        price: docs.price,
                        _id: docs._id,
                        request: {
                            type: "GET",
                            url: "http://localhost:3000/products/" + docs._id
                        }
                    }
                })

              }
              res.status(200).json(response)
           })
           .catch(err => {
               console.log(err)
               res.status(500).json({error : err})
           })
})

//POST method working on /product
router.post('/', upload.single('productImage') ,(req,res,next)=>{
    console.log(req.file)
    //taking product name and price and posting them
    const product = new Product( {
        //adding as schema in DB
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,//name of product needed in body
        price: req.body.price//price of product needed in body
    })
    //Saving data using .save() then using as promise .catch used for catching error every thing provided by mongose. 
    product
    .save()
    .then(result => {
        console.log(result)
        res.status(201).json({
            message: "Created Product Successfully",
            product:{
                name: result.name,
                price: result.price,
                id: result._id,
                //request to redirect to diffrent place
                request: {
                    type: 'GET',
                    url: "http://localhost:3000/products/" + result._id
                }
            }
        })
    })
    .catch(err => console.log(err) )

})

//GET data using variable method 
router.get('/:_id',(req,res,next)=>{

    const id = req.params._id;
    Product.findById(id)
            .select('name price _id')
           .exec()
           .then(doc=> {
                console.log(doc)
               if(doc){
                res.status(200).json({
                    product: doc,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products'
                    }
                })
               }
               else{
                   res.status(404).json({message:'ID not Avalaibale'})
               }
                
           })
           .catch(err => {
               console.log(err)
               res.status(500).json({error : err})
           })
        
  
})

//Patch data using variable id
router.patch('/:productID',(req,res,next)=>{
    const id= req.params.productID;
    const updateOps ={};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    Product.updateOne({_id:id},{$set:updateOps})
            .exec()
            .then(result => {
                console.log(result)
                res.status(200).json({
                    message: 'Product updated',
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/' + id
                     }
                })
                })
            .catch(err =>{
                console.log(err)
                res.status(500).json({error : err})
            })
})

//delete using variable id
router.delete('/:productID',(req,res,next)=>{
    const id= req.params.productID;
    Product.remove({_id: id})
        .exec()
        .then(result =>{
            res.status(200).json({
                message: 'Product deleted',
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/products',
                    body: { name: 'String', price: 'Number' }
                }
            })
        })
        .catch(error=>{
            console.log(error)
            res.status(500).json({
                error
            })
        })
})


//exporting router updates to app
module.exports = router;