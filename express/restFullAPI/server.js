//http requtes to folder
const http = require('http');
//adding app.js as main file
const app = require('./app');
//sever port 3000
const port = process.env.PORT || 3000;
//app working as method when server is called
const server = http.createServer(app);

//listens to post request
server.listen(port);

