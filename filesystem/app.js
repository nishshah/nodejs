const fs = require('fs')
const express = require('express')
const bodyparser = require('body-parser')
const { json } = require('body-parser')
const multer = require('multer')
const path = require('path')

const app = express()
//set storege engine
const storage = multer.diskStorage({
    destination: './uploads/',
    filename: (req,file,cb)=>{
        cb(null,path.basename(file.originalname)+'-'+Date.now()+path.extname(file.originalname))
    }
}) 

//init upload
const upload = multer({
    storage: storage,
    limits:{
        fileSize: 1000000
    },
    fileFilter: (req,file,cb)=>{
        checkfiletype(file,cb)
    }
}).single('image')


const checkfiletype = (file, cb)=>{
     //allowed ext
     const filetypes = / |jpeg|jpg|png|gif /
     //check ext
     const ext = filetypes.test(path.extname(file.originalname).toLowerCase())
     const mimetype = filetypes.test(file.mimetype)

     if(mimetype && ext){
         return cb(null, true)
     }
     else{
         cb("error images only!")
     }
}

app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())
app.use(express.static('./uploads'))

app.get('/',(req,res)=>{
    const data ={
            name: req.body.name,
            moblie: req.body.number
        }
        const dataJson = JSON.stringify(data)
        datasave(dataJson)
        
        res.status(200).json({
            name: req.body.name,
            moblie: req.body.number
        })
   
})

app.post('/upload',(req,res)=>{
    upload(req,res,(err)=>{
        if(err){res.send(err)}
        else{
            if(req.file == undefined){
                res.send({
                    message:"No image selected! Use below URL to upload",
                    url:'localhost:3000/upload'
                })
            }
            else{
                res.status(200).send('Image Uploaded')
            }
        }
    })
})



app.get('/updata',(req,res)=>{
    const indata = {
        name: req.body.name,
        moblie: req.body.number
    }
    const data = JSON.stringify(indata)
    JSON.parse(data)
    console.log(data)
    fs.appendFileSync('ex.json',JSON.parse(data))
    res.status(200).send(indata)
})

let datasave = (data)=>{
    fs.writeFileSync('ex.json',data)
}
app.get('/data',(req,res)=>{
        const dataJson = loaddata()
        console.log(dataJson)
            res.status(200).json(dataJson)
        
})

const loaddata = ()=>{
    const dataBuffer =fs.readFileSync('ex.json')
    const dataJson = dataBuffer.toString()
    return JSON.parse(dataJson)
}

app.listen(3000,()=> console.log("Server is on!"))