const express = require('express')
const swaggerUI = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')

const options ={
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Example API",
            version:"1.0.0",
            description:"Swagger Example API"
        },
        servers:[
            {
                url:'http://localhost:3000'
            }
        ],
    },
    apis: ['./routes/*.js']
}

const specs = swaggerJSDoc(options)
const app = express()
const profile = require('./routes/profile')

app.use('/api',swaggerUI.serve, swaggerUI.setup(specs))
app.use(express.json())
app.use('/ex',profile)

 module.exports =  app.listen(3000,()=>{
    console.log("Server is on PORT #3000")
})