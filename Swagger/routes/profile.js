const express = require('express')
const router = express.Router()

/**
 * @swagger
 * components:
 *  schemas:
 *      Profile:
 *          type: object
 *          requried:
 *            -name
 *            -age
 *            -mail
 *          properties:
 *              name:
 *                  type: string
 *                  description: Enter your name
 *              age:
 *                  type: integer
 *                  description: enter you age
 *              mail:
 *                  type: string
 *                  description: Enter Email
 */

/**
 * @swagger
 *   /ex:
 *      get:
 *          summary: Gets response
 *          responses:
 *              '200':
 *               description: Get Output
 *               content:
 *                  application/json:
 *                      schema:
 *                        type: array
 *                        items:
 *                          $ref: '#/components/schemas/Profile' 
 */
router.get('/',(req,res)=>{
    const { name, age , mail } = {name: "Nishant Shah",age: 22,mail: "shah@nishant"}

    res.json({name, age, mail})
})

/**
 * @swagger
 * /ex/add:
 *  post:
 *      summary: Post response
 *      requestBody:
 *         require: true
 *         content:
 *             application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/Profile'
 *      responses:
 *          200:
 *              description: Added Details
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/Profile'
 */

router.post('/add',(req,res)=>{
    const { name,age,mail } = req.body

    res.status(200).json({name,age,mail})
})

module.exports = router