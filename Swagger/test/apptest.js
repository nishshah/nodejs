const assert = require('assert');
const app = require("../app");
const request = require("supertest");

describe( "Test1", () => {
    beforeEach(() => {
      console.log( "executes before every test" );
    });
      
    it("Is returning 5 when adding 2 + 3", () => {
      assert.equal(2 + 3, 5);
    });
  
    it("Is returning 6 when multiplying 2 * 3", () => {
      assert.equal(2 * 3, 6);
    });
  });

  describe("routes test", () => {
	describe("Get route", () => {
		beforeEach(() => {
			console.log("checking Get route");
		});

		it(`Is Giving data`,(done)=> {
			request(app)
				.get("/ex/")
				.expect(200)
				.then((res) => {
					assert(res.text, {"name": "Nishant Shah","age": 22,"mail": "shah@nishant"});
					done();
				})
				.catch((err) => done(err));
		})
	})
  })
