const express = require('express')

const app=express()

//noting on url(root page)
app.get('',(req , res)=>{
    res.send('Hello World!')
})
// /help on url
app.get('/help',(req , res) => {
    res.send('Help Page!')
})
// /about on url
app.get('/about',(req , res) =>{
    res.send('About Page!')
})
// /weather route page
app.get('/weather',(req , res) =>{
    res.send('Will Show Weather!')
})



app.listen(3000 , ()=>{
    console.log('Sever is on!')
})