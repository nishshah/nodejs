let ejs = require('ejs')
let express = require('express')
let app = express()



//set view engine to ejs
app.set('view engine', 'ejs')

//noting on url(root page)
app.get('',(req , res)=>{
  res.render('index.ejs')
})
// /help on url
app.get('/',(req , res) => {
  res.send('index.ejs')
})
// /about on url
app.get('/about',(req , res) =>{
  res.render('about.ejs')
})

app.listen(3000 , ()=>{
  console.log('Sever is on!')
})