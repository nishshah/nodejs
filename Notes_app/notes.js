const fs = require('fs')
const chalk=require('chalk');
const { title } = require('process');
let getNotes = (str) => {
    return str;
}

const addNote = (title, body) => {
  const notes = loadNotes()
  const duplicateNote = notes.find((note) => note.title === title)
  if(!duplicateNote){
    notes.push({
        title : title,
        body: body
    })
    saveNotes(notes)
    console.log('New note Added!')
  }
  else{
      console.log('Title Taken!')
  }


}

const removeNote = (title) =>{
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) =>{
        return note.title !== title
      })
      if(notes.length > notesToKeep.length){
          console.log(chalk.inverse.bold.green("Note Removed!"))
      }
      else{
        console.log(chalk.inverse.bold.red("No Note found!"))
      }
      saveNotes(notesToKeep)
}

const listNodes = () =>{
    const notes =loadNotes()
    console.log(chalk.inverse.blue.bold("Your Notes"));
    notes.forEach((note) => {
      console.log(note.title)  
    });
}
const readNote = (title) => {
    const notes = loadNotes()
    const note = notes.find((note) => note.title === title)

    if (note) {
        console.log(chalk.inverse(note.title))
        console.log(note.body)
    } else {
        console.log(chalk.red.inverse('Note not found!'))
    }
}

const saveNotes = (notes) => {
    const dataJson = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJson)
}

const loadNotes = () =>  {
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJson = dataBuffer.toString()
        return JSON.parse(dataJson)
    }
    catch(e){
        return []
    }
}

module.exports = {
    getNotes : getNotes,
    addNote : addNote,
    removeNote : removeNote,
    listNodes : listNodes,
    readNote : readNote
}