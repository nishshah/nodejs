let chalk=require('chalk')
const { command, argv } = require('yargs')
const yargs = require('yargs')
const notes = require('./notes')

//yargs version
yargs.version('1.1.0')

//yargs command 
//creating add command
yargs.command({
    command: 'add',
    describe: 'Adding New Note!',
    builder:{
        title:{
        describe:'Note Title',
        demanandOption : true,
        type: 'string'
        },
        body:{
            describe:'Add Body',
            demanandOption : true,
            type: 'string'
        }
    },
    handler : ()=>{
        notes.addNote(argv.title, argv.body)
        
    }

})

//creating remove command
yargs.command({
    command: 'remove',
    describe: 'Removeing New Note!',
    builder:{
        title:{
        describe:'Note Title',
        demanandOption : true,
        type: 'string'
        }
    },
    handler : ()=>{
        notes.removeNote(argv.title)
    }

})

//creating list command
yargs.command({
    command: 'list',
    describe: 'List of all Notes!',
    handler : ()=>{
        notes.listNodes()
    }

})

//creating read command
yargs.command({
    command: 'read',
    describe: 'Read a note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv) {
        notes.readNote(argv.title)
    }
})
yargs.parse();