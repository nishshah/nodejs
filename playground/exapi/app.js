const express = require('express')
const crsf = require('csurf')
const cookieparser = require('cookie-parser')
const session = require('express-session')
const helmet = require('helmet')
const bodyparser = require('body-parser')
const i18next = require('i18next')
const backend = require('i18next-fs-backend')
const middleware = require('i18next-http-middleware')


//adding i18next configuration
i18next.use(backend).use(middleware.LanguageDetector)
.init({
    //by default english is selected
    fallbackLng: 'en',
    backend:{
        //{{lng}} will check from the header with lng is selected
        //will check in folder local the laguge selected and the json file try to keep all names similar
        loadPath: './local/{{lng}}/tran.json'
    }
})
const app = express()

const parse = bodyparser.urlencoded({extended:false})
const user ={
    email: "nishant1@gmail.com",
    password: "password"
}
//addind internalization
app.use(middleware.handle(i18next))

//adding csrf protection
const csrfprotection = crsf({cookie : true})
//using helmet
app.use(helmet())
//adding cookie parser with csrf
app.use(cookieparser())
//created session if user is logged in
app.use(session({
    secret: 'boom',
    resave:false,
    saveUninitialized: false
   
}))
app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())

app.get('/',(req,res)=>{
    res.status(200).json({
        message: req.t('message'),
        //req stores the information which we can get through req.t('name-display')
        user1: req.t('user1'),
        //user nishant
        url: "localhost:3000/login",
        user2: req.t('user2'),
        //random user
        url2: 'localhost:3000/form'
    })
})
//create csrf token
app.get('/form', csrfprotection, (req , res)=>{
    
    res.status(200).json({ 
        csrfToken: req.csrfToken()+ req.t('token'),
        email: req.body.email,
        name: req.body.name,
        url: 'localhost:3000/normal'
    })
})
//use csrftoken to enter other than cant login to this page
app.get('/normal',parse,csrfprotection,(req,res)=>{
    res.status(200).json({
        message: req.t('nrmess')
    })
})

app.get('/login',(req,res)=>{
    if(user.email === req.body.email && user.password === req.body.password)
    {
        //authticte only avaliable to the user with user id password
            req.session.isAuth=true;
            res.status(200).json({
            message:req.t('hello'),
            email: req.body.email,
            session: req.session.id,           
        })
             
    }
    else{
        res.status(404).json({
             message: req.t('ermess'),
             url : "localhost:3000/login"
            })
    }
})


app.post('/logout',(req,res)=>{
    console.log(req.session.id)
    //deleteing session
    req.session.destroy((err)=>{
        if(err){res.json(err)}
        else{ res.send(req.t('sesEnd')) } 
    })
})

app.listen(3000,()=>{
    console.log('Server is On!')
})