const express = require('express')
const i18next = require('i18next')
const backend = require('i18next-fs-backend')
const middleware = require('i18next-http-middleware')
const app = express()
i18next.use(backend).use(middleware.LanguageDetector)
.init({
    fallbackLng: 'en',
    backend:{
        loadPath: './local/{{lng}}/tran.json'
    }
})
app.use(middleware.handle(i18next))

app.get('/',(req,res)=>{
    res.status(200).send({ message: req.t('message')})
})

app.listen(8080,()=>{
    console.log("Server is on at 8080")
})
