/*created a :
    GET/form with noting as input which will provide and output of csrf number
      because we have have csrf protection as true.
  Then the POST/process will try to featch the data 
  1-> without csrf code as body or header
   --This will show an error as only csrf authenticated and request it.
  2-> with csrf code taken from the output of GET/form will send in body or header with '_csrf'
   --This wont show a error and will perform the output. */

const cookieparser = require('cookie-parser')
const crsf = require('csurf')
const bodyparser = require('body-parser')
const express = require('express')

//seting up route using cookie true
const csrfprotection = crsf({cookie : true})
const parse = bodyparser.urlencoded({extended:false})

//express app
const app= express()

//setting app to use cookie instead of session
app.use(cookieparser())

//passing csrf token to view
app.get('/form', csrfprotection, (req , res)=>{
    
    res.send({ csrfToken: req.csrfToken()})
})

app.post('/proccess', parse,csrfprotection,(req,res)=>{
    res.send('Data is being processed!')
})

app.listen(3000,()=>{
    console.log("Server is on!")
})