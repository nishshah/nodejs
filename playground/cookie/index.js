const express = require('express')
const cookie = require('cookie-parser')

const app=express()

app.use(cookie())

app.get('/',(req,res)=>{
    res.cookie('ExCookie','nishant Shah').send('Cookie has been set.')
    // res.cookie('expcookie','Expire',{expire: 22000}).send('expire cookie is set!')
} )
app.get('/show',(req,res)=>{
    res.send(req.cookies)
})
app.listen(5000,()=>{
    console.log('Server is on!')
})