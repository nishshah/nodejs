const express = require('express')
const helmet = require('helmet')
const app = express()

app.use(helmet())
app.get('/',(req,res) =>{
    res.send("<h1>Hello Helmet</h1>")
})

app.listen(3000,()=>{
    console.log("Server is ON!")
})