// Formate of token
// authorization : Berar <accsess_token>

const express = require('express')
const jwt = require('jsonwebtoken')

const app = express()



app.get('/', (req,res)=>{
     res.json({
         message: "Welcome Api"
     })
})

app.post('/post',verfiyToken ,(req,res)=>{
    jwt.verify(req.token , 'key',(error, authData)=>{
        if(error){
            res.send(error)
        }
        else{
            res.json({
                
                    message: "post created!"
                })
           
        }
    })
 
})

app.post('/login',(req,res)=>{
    //mock user
    const user={
        id: 1,
        name:"Nishant",
        email:"nishant@gmail.com"
    }
    jwt.sign({user},'key',(err,token)=>{
        res.json({token})
    })
})

function verfiyToken(req,res,next){
    //getting auth value
    const berarerHeader = req.headers['authorization']
    
    if(typeof berarerHeader !== 'undefined'){
        //split and get <auth_token>
        //split with split the hearer to array and the(' ')space in the () is the char it will split at
        const berar = berarerHeader.split(' ')
        //get token from arry
        const gettoken = berar[1]
        //setting token to verfiy
        req.token = gettoken
        //calling next token
        next()
    }
    else{
        res.status(403).send('Forbidden!')
    }
}

app.listen(3000,()=>{
    console.log("Server Started!!")
})